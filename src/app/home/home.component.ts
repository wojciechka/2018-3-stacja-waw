import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'st-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @Input() owner = 'tomek';
  @Output() timePassed = new EventEmitter<number>();

  constructor() {
    setInterval(() => {
      this.timePassed.emit(+new Date());
    }, 2500);
  }

  ngOnInit() {
  }
}
