import {Component, OnInit} from '@angular/core';
import {TvmazeService} from '../tvmaze.service';
import {Show} from '../tv.models';
import {Observable} from 'rxjs/Observable';
import {timer} from 'rxjs/observable/timer';

@Component({
  selector: 'st-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  query = 'flash';
  // shows: Show[] = [];
  shows$: Observable<Show[]>;
  timer$: Observable<any>;

  constructor(private tv: TvmazeService) {
    this.timer$ = timer(0, 1000);
  }

  ngOnInit() {
  }

  search(query: string) {
    this.shows$ = this.tv.searchShows(query);
      // .subscribe(shows => this.shows = shows);
  }

}
